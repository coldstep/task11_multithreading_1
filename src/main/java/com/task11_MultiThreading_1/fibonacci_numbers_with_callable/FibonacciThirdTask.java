package com.task11_MultiThreading_1.fibonacci_numbers_with_callable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.stream.Collectors;

public class FibonacciThirdTask {

  private static final List<Long> list = Collections.synchronizedList(new ArrayList<>());
  private static volatile int size;
  private List<Future<Long>> listOfFutureObject = new ArrayList<>();
  private ExecutorService executor;


  public FibonacciThirdTask(int sizeOfSequence) {
    size = sizeOfSequence - 1;
    list.add((long) 1);
  }

  public void start() {
    createThreads(size);
    finish();
    while (!executor.isTerminated()) {
    }
    List<Long> tempList = new ArrayList<>();
    tempList.add((long) 1);
    try {
      for (Future<Long> longFuture : listOfFutureObject) {
        Long aLong = longFuture.get();
        tempList.add(aLong);
      }
    } catch (InterruptedException | ExecutionException e) {
      e.printStackTrace();
    }
    System.out.print("Fibonacci sequence:\n{ ");
    list.forEach((a) -> System.out.print(a + ", "));
    System.out.print("}\n");
    System.out.println("Sum of sequences - " + tempList.stream()
        .collect(Collectors.summarizingLong(Long::longValue)));
  }

  private void createThreads(int numberOfThreads) {
    executor = Executors.newFixedThreadPool(numberOfThreads);
    for (int i = 0; i < numberOfThreads; i++) {
      listOfFutureObject.add(executor.submit(this::calculateFibonacciSequence));
    }
  }

  private void finish() {
    executor.shutdown();
  }

  private long calculateFibonacciSequence() {
    synchronized (list) {
      System.out.println(Thread.currentThread().getName());
      long temp = list.get(list.size() - 1);
      if (list.size() == 1) {
        list.add(temp);
      } else {
        temp = list.get(list.size() - 1);
        temp += list.get(list.size() - 2);
        list.add(temp);
      }
      return temp;
    }
  }

  public static void main(String[] args) {
    System.out.print("Please write number of size fibonacci sequence:\nsize - ");
    new FibonacciThirdTask(new Scanner(System.in).nextInt()).start();
  }
}
