package com.task11_MultiThreading_1.fibonacci_number_with_executor;


import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FibonacciSecondTask {

  private static final List<Long> list = Collections.synchronizedList(new ArrayList<>());
  private static volatile int size;
  private ExecutorService executor;


  public FibonacciSecondTask(int sizeOfSequence) {
    size = sizeOfSequence - 1;
    list.add((long) 1);
  }

  public void start() {
    createThreads(size);
    finish();
    while (!executor.isTerminated()) {
    }
    System.out.print("Fibonacci sequence:\n{ ");
    list.forEach((a) -> System.out.print(a + ", "));
    System.out.print("}");
  }

  private void createThreads(int numberOfThreads) {
    executor = Executors.newFixedThreadPool(numberOfThreads);
    for (int i = 0; i < numberOfThreads; i++) {
      executor.execute(this::calculateFibonacciSequence);
    }
  }

  private void finish() {
    executor.shutdown();
  }

  private void calculateFibonacciSequence() {
    synchronized (list) {
      System.out.println(Thread.currentThread().getName());
      long temp = list.get(list.size() - 1);
      if (list.size() == 1) {
        list.add(temp);
      } else {
        temp = list.get(list.size() - 1);
        temp += list.get(list.size() - 2);
        list.add(temp);
      }
    }
  }

  public static void main(String[] args) {
    System.out.print("Please write number of size fibonacci sequence:\nsize - ");
    new FibonacciSecondTask(new Scanner(System.in).nextInt()).start();
  }
}
