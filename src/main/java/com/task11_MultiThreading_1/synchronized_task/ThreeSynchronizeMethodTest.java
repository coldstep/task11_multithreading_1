package com.task11_MultiThreading_1.synchronized_task;


import java.util.Scanner;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ThreeSynchronizeMethodTest {

  private static long sum = 0;
  private static final Object object = new Object();
  private ExecutorService executor;

  public ThreeSynchronizeMethodTest(int numberOfThreads) {
    createThreads(numberOfThreads);
    while (!executor.isTerminated()) {
    }
    System.out.println("All threads are finished sum - " + sum);
  }

  private void firstMethod() {
    synchronized (object) {
      System.out.println("Start firstMethod");
      System.out.println("  "+Thread.currentThread().getName());
      for (int i = 0; i < 10000; i++) {
        sum ++;
      }
      System.out.println("  sum - "+sum);
      System.out.println("Finish firstMethod ");
    }
  }

  private void secondMethod() {
    synchronized (object) {
      System.out.println("Start secondMethod");
      System.out.println("  "+Thread.currentThread().getName());
      for (int i = 0; i < 10000; i++) {
        sum ++;
      }
      System.out.println("  sum - "+sum);
      System.out.println("Finish secondMethod ");
    }
  }

  private void thirdMethod() {
    synchronized (object) {
      System.out.println("Start thirdMethod");
      System.out.println("  "+Thread.currentThread().getName());
      for (int i = 0; i < 10000; i++) {
        sum ++;
      }
      System.out.println("  sum - "+sum);
      System.out.println("Finish thirdMethod ");
    }
  }

  private void createThreads(int numberOfThreads) {
    Runnable runnable = () -> {
      firstMethod();
      secondMethod();
      thirdMethod();

    };
    executor = Executors.newFixedThreadPool(numberOfThreads);
    for (int i = 0; i < numberOfThreads; i++) {
      executor.execute(runnable);
    }
    finish();
  }

  private void finish() {
    executor.shutdown();
  }

  public static void main(String[] args) {
    System.out.print("Please write number of threads - ");
    new ThreeSynchronizeMethodTest(new Scanner(System.in).nextInt());

  }
}
